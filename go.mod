module gitlab.com/dwagin/go-syslog

go 1.23

require (
	github.com/stretchr/testify v1.10.0
	gitlab.com/dwagin/go-bytebuffer v1.4.6
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
