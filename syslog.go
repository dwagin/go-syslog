package syslog

import (
	"errors"
	"fmt"
	"net"
	"os"
	"path"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/dwagin/go-bytebuffer"
)

// The Priority is a combination of the syslog facility and
// severity. For example, LOG_ALERT | LOG_FTP sends an alert severity
// message from the FTP facility. The default severity is LOG_EMERG;
// the default facility is LOG_USER.
type Priority int

const (
	severityMask = 0x07
	facilityMask = 0xf8
)

//nolint:revive,stylecheck // corresponds to /usr/include/sys/syslog.h
const (
	// Severity.

	// From /usr/include/sys/syslog.h.
	// These are the same on Linux, BSD, and OS X.

	LOG_EMERG Priority = iota
	LOG_ALERT
	LOG_CRIT
	LOG_ERR
	LOG_WARNING
	LOG_NOTICE
	LOG_INFO
	LOG_DEBUG
)

//nolint:revive,stylecheck // corresponds to /usr/include/sys/syslog.h
const (
	// Facility.

	// From /usr/include/sys/syslog.h.
	// These are the same up to LOG_FTP on Linux, BSD, and OS X.

	LOG_KERN Priority = iota << 3
	LOG_USER
	LOG_MAIL
	LOG_DAEMON
	LOG_AUTH
	LOG_SYSLOG
	LOG_LPR
	LOG_NEWS
	LOG_UUCP
	LOG_CRON
	LOG_AUTHPRIV
	LOG_FTP
	_ // unused
	_ // unused
	_ // unused
	_ // unused
	LOG_LOCAL0
	LOG_LOCAL1
	LOG_LOCAL2
	LOG_LOCAL3
	LOG_LOCAL4
	LOG_LOCAL5
	LOG_LOCAL6
	LOG_LOCAL7
)

const maxLine = 8192

var bbPool = bytebuffer.NewPool()

type Writer struct {
	conn     net.Conn
	hostname string
	network  string
	raddr    string
	tag      string
	priority Priority
	pid      int64
	mu       sync.Mutex
}

func New(options ...Option) (*Writer, error) {
	var err error

	cfg := config{}
	for i := range options {
		options[i](&cfg)
	}

	if (cfg.priority & ^(facilityMask | severityMask)) > 0 {
		return nil, fmt.Errorf("unknown facility/priority: %x", cfg.priority)
	}

	if (cfg.priority & facilityMask) == 0 {
		// This is the default facility identifier if none is specified.
		cfg.priority |= LOG_USER
	}

	if cfg.tag == "" {
		cfg.tag = path.Base(os.Args[0])
	}

	if cfg.hostname, err = os.Hostname(); err != nil {
		return nil, fmt.Errorf("hostname error: %v", err)
	}

	switch cfg.network {
	case "", "unixgram", "unix", "udp", "udp4", "udp6":
	default:
		return nil, fmt.Errorf("unsupported network %q", cfg.network)
	}

	v := &Writer{
		priority: cfg.priority,
		hostname: cfg.hostname,
		tag:      cfg.tag,
		pid:      int64(os.Getpid()),
		network:  cfg.network,
		raddr:    cfg.raddr,
	}

	if err = v.connect(); err != nil {
		return nil, err
	}

	return v, nil
}

func (v *Writer) Close() error {
	v.mu.Lock()
	defer v.mu.Unlock()

	return v.close()
}

func (v *Writer) Write(p Priority, msg []byte) error {
	var err error

	p = (v.priority & facilityMask) | (p & severityMask)

	bb := bbPool.Get()
	defer bb.Release()

	bb.B = v.format(bb.B, p, msg)

	v.mu.Lock()
	defer v.mu.Unlock()

	if v.conn == nil {
		if err = v.connect(); err != nil {
			return err
		}
	}

	err = v.write(bb.B)

	return err
}

func (v *Writer) connect() error {
	v.close()

	var (
		err error
		c   net.Conn
	)

	switch v.network {
	case "":
		if c, err = unixConnect(); err != nil {
			return err
		}

		if v.hostname == "" {
			v.hostname = "localhost"
		}
	default:
		if c, err = net.Dial(v.network, v.raddr); err != nil {
			return fmt.Errorf("dial error: %v", err)
		}

		if v.hostname == "" {
			v.hostname = c.LocalAddr().String()
		}
	}

	v.conn = c

	return nil
}

func (v *Writer) close() error {
	if v.conn != nil {
		c := v.conn
		v.conn = nil

		if err := c.Close(); err != nil {
			return fmt.Errorf("close error: %v", err)
		}
	}

	return nil
}

const rfc3339Micro = "2006-01-02T15:04:05.999999Z07:00"

func (v *Writer) format(dst []byte, p Priority, msg []byte) []byte {
	// Priority
	dst = append(dst, '<')
	dst = strconv.AppendInt(dst, int64(p), 10)
	dst = append(dst, ">1 "...)

	// Timestamp
	dst = time.Now().AppendFormat(dst, rfc3339Micro)
	dst = append(dst, ' ')

	// Hostname
	dst = append(dst, v.hostname...)
	dst = append(dst, ' ')

	// Tag
	dst = append(dst, v.tag...)
	dst = append(dst, ' ')

	// PID
	dst = strconv.AppendInt(dst, v.pid, 10)

	// Message ID, Structured data
	dst = append(dst, " - - "...)

	// Message
	if len(msg) >= 1 && msg[len(msg)-1] == '\n' {
		msg = msg[:len(msg)-1]
	}

	dst = append(dst, msg...)

	return dst
}

func (v *Writer) write(buf []byte) error {
	_, err := v.conn.Write(buf)
	if err != nil {
		if len(buf) > maxLine && errors.Is(err, syscall.EMSGSIZE) {
			return v.write(buf[:maxLine])
		}

		if !errors.Is(err, syscall.ENOBUFS) {
			v.close()
		}

		return fmt.Errorf("write error: %w", err)
	}

	return nil
}
