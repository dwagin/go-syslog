package syslog_test

import (
	"bytes"
	"syscall"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-syslog"
)

func TestSyslog(t *testing.T) {
	require := require.New(t)

	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(err)
	defer sl.Close()

	err = sl.Write(syslog.LOG_WARNING, []byte("test"))
	require.NoError(err)
}

func TestSyslogLong(t *testing.T) {
	require := require.New(t)

	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(err)
	defer sl.Close()

	err = sl.Write(syslog.LOG_WARNING, bytes.Repeat([]byte("test "), 2500))
	require.NoError(err)
}

func TestSyslogWithNewLine(t *testing.T) {
	require := require.New(t)

	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(err)
	defer sl.Close()

	err = sl.Write(syslog.LOG_WARNING, []byte("test\n"))
	require.NoError(err)
}

func BenchmarkSyslog(b *testing.B) {
	require := require.New(b)

	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(err)
	defer sl.Close()

	b.ReportAllocs()
	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			err = sl.Write(syslog.LOG_WARNING, []byte("test"))
			if err != nil {
				require.ErrorIs(err, syscall.ENOBUFS)
			}
		}
	})
}
